'''
OpenMV4P_micro
拍照程序

说明：
程序读取/photos路径下的.bmp格式的.bin缩略图显示。如果bmp没有对应的bin，则会自动生成。
支持电脑存入的bmp显示。
按左键向左翻页，按右键向右翻页
长按左或右键一秒以上，快速翻页

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220906
'''
import sensor, image, time, screen, button
from os import listdir
from pyb import millis
from math import floor
clock = time.clock()
screen=screen.screen()

file_names=listdir('/photos')#扫描照片目录
file_names.sort()#文件排序
bmp_file_names=listdir('/photos')#扫描照片目录
bmp_file_names.sort()#文件排序
n=0
while n < len(bmp_file_names)-1:#删除非标准格式命名的照片文件
    for n in range(len(bmp_file_names)):
        if not bmp_file_names[n][-4:]=='.bmp':
            del bmp_file_names[n]
            break
#上一节程序，如果倒数第二个文件不是py，删除后不会遍历最后一个文件，下面再遍历一遍解决这个BUG
for n in range(len(bmp_file_names)):
    if not bmp_file_names[n][-4:]=='.bmp':
        del bmp_file_names[n]
        break

#没有照片抛出错误
if len(bmp_file_names)==0:
    raise Exception('No photos to view!')

#生成缩略图
img_display=sensor.alloc_extra_fb(320,172,sensor.RGB565)#显示所用画面
for n in range(len(bmp_file_names)):
    try:
        file_names.index(bmp_file_names[n][:-4]+'.bin')
    except:
        img_display.clear()
        img_display.draw_image(image.Image('/photos/'+bmp_file_names[n],copy_to_fb=True),0,0,y_size=172)
        stream = image.ImageIO('/photos/'+bmp_file_names[n][:-4]+'.bin', "w")
        stream.write(img_display)
        stream.close()
        img_display.draw_rectangle(0,76,320,20,color=(150,150,150),fill=True)
        img_display.draw_string(10,76,'Thumbnails are being generated...',scale=2,x_spacing=1,mono_space=False)
        screen.display(img_display)

select=len(bmp_file_names)-1#当前所选照片序号

img_photo_1=sensor.alloc_extra_fb(320,172,sensor.RGB565)#预加载的照片，前后各一张，用于翻页动效
img_photo_2=sensor.alloc_extra_fb(320,172,sensor.RGB565)
img_photo_3=sensor.alloc_extra_fb(320,172,sensor.RGB565)
img_photo_temp=sensor.alloc_extra_fb(320,172,sensor.RGB565) #临时加载所用画布。因为读取bin会加载到特定空间，而这个空间是惟一的。通过临时画布再转存才能保存不同图像。
try:    #加载初始的两张照片
    stream = image.ImageIO('/photos/'+bmp_file_names[select][:-4]+'.bin', "r")
    img_photo_temp = stream.read(copy_to_fb=True)
    stream.close()
    img_photo_2.draw_image(img_photo_temp,0,0)
    stream = image.ImageIO('/photos/'+bmp_file_names[select-1][:-4]+'.bin', "r")
    img_photo_temp = stream.read(copy_to_fb=True)
    stream.close()
    img_photo_1.draw_image(img_photo_temp,0,0)
except:
    pass

#翻页后，提前加载前中后照片
def reload_photos(direction):
    global img_photo_1,img_photo_2,img_photo_3,select,bmp_file_names
    if direction=='left':#左键，上一张照片
        img_photo_1.clear()
        img_photo_1.draw_image(img_photo_2,0,0)
        img_photo_2.clear()
        img_photo_2.draw_image(img_photo_3,0,0)
        img_photo_3.clear()
        if select<len(bmp_file_names)-1:
            try:
                stream = image.ImageIO('/photos/'+bmp_file_names[select+1][:-4]+'.bin', "r")
                img_photo_temp = stream.read(copy_to_fb=True)
                stream.close()
                img_photo_3.draw_image(img_photo_temp,0,0)
            except:
                pass
    if direction=='right':#左键，上一张照片
        img_photo_3.clear()
        img_photo_3.draw_image(img_photo_2,0,0)
        img_photo_2.clear()
        img_photo_2.draw_image(img_photo_1,0,0)
        img_photo_1.clear()
        if select>0:
            try:
                stream = image.ImageIO('/photos/'+bmp_file_names[select-1][:-4]+'.bin', "r")
                img_photo_temp = stream.read(copy_to_fb=True)
                stream.close()
                img_photo_1.draw_image(img_photo_temp,0,0)
            except:
                pass
#首次进入的显示
img_display.clear()
stream = image.ImageIO('/photos/'+bmp_file_names[select][:-4]+'.bin', "r")
img_photo_temp = stream.read(copy_to_fb=True)
stream.close()
img_display.draw_image(img_photo_temp,0,0)
img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
screen.display(img_display)

button_hold_timer=millis()  #用于判断长按的计时
#主循环
while True:
#左键，翻页序号增加
    if button.left.state():
        time.sleep(0.05)
        button_hold_timer=millis()
        while button.left.state():
            if millis()-button_hold_timer>1000: #长按
                speed=floor((millis()-button_hold_timer)/200)+10#每个0.2秒速度累加1
                select+=1   #选择累加
                if select<len(bmp_file_names):  #在范围内
                    for n in range(0,321,speed):    #翻页动效，用预先缓存好的照片，滚动填充到显示的画布
                        img_display.draw_image(img_photo_2,-n,0)
                        img_display.draw_image(img_photo_3,320-n,0)
                        img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                        img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                        screen.display(img_display)
                    img_display.draw_image(img_photo_3,0,0)
                    img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                    img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                    screen.display(img_display)
                    reload_photos('left')   #重新预加载
                else:
                    select-=1
                    for n in range(3):
                        img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                        img_display.draw_string(18,10,str(select+1),color=(255,0,0),scale=2,x_spacing=1,mono_space=False)
                        screen.display(img_display)
                        time.sleep(0.05)
                        img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                        img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                        screen.display(img_display)
                        time.sleep(0.05)

        select+=1
        if select<len(bmp_file_names):
            for n in range(0,321,10):
                img_display.draw_image(img_photo_2,-n,0)
                img_display.draw_image(img_photo_3,320-n,0)
                img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                screen.display(img_display)
            reload_photos('left')
        else:
            select-=1
            for n in range(3):
                img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                img_display.draw_string(18,10,str(select+1),color=(255,0,0),scale=2,x_spacing=1,mono_space=False)
                screen.display(img_display)
                time.sleep(0.05)
                img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                screen.display(img_display)
                time.sleep(0.05)
#右键，翻页序号减少
    if button.right.state():
        time.sleep(0.05)
        button_hold_timer=millis()
        while button.right.state():
            if millis()-button_hold_timer>1000:
                speed=floor((millis()-button_hold_timer)/500)+10
                select-=1
                if select>=0:
                    for n in range(0,321,speed):
                        img_display.draw_image(img_photo_2,+n,0)
                        img_display.draw_image(img_photo_1,n-320,0)
                        img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                        img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                        screen.display(img_display)
                    img_display.draw_image(img_photo_1,0,0)
                    img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                    img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                    screen.display(img_display)
                    reload_photos('right')
                else:
                    select+=1
                    for n in range(3):
                        img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                        img_display.draw_string(18,10,str(select+1),color=(255,0,0),scale=2,x_spacing=1,mono_space=False)
                        screen.display(img_display)
                        time.sleep(0.05)
                        img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                        img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                        screen.display(img_display)
                        time.sleep(0.05)
        select-=1
        if select>=0:
            for n in range(0,321,10):
                img_display.draw_image(img_photo_2,+n,0)
                img_display.draw_image(img_photo_1,n-320,0)
                img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                screen.display(img_display)
            reload_photos('right')
        else:
            select+=1
            for n in range(3):
                img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                img_display.draw_string(18,10,str(select+1),color=(255,0,0),scale=2,x_spacing=1,mono_space=False)
                screen.display(img_display)
                time.sleep(0.05)
                img_display.draw_string(19,11,str(select+1),scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                img_display.draw_string(18,10,str(select+1),scale=2,x_spacing=1,mono_space=False)
                screen.display(img_display)
                time.sleep(0.05)
