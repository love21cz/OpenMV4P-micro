'''
OpenMV4P_micro
手表展示程序

最简化的一个界面，没有设置功能。
展现设备的可能性~

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220909
'''
import sensor, image, time,screen,button
from pyb import RTC

rtc = RTC()
rtc.datetime((2022, 9, 9, 5, 21, 30, 56, 0))

img = sensor.alloc_extra_fb(320,172, sensor.RGB565)

screen=screen.screen()
placeholder=['','','']
while True:
    img.clear()
    time_data=rtc.datetime()

    for n in range(4,7):
        if time_data[n]<10:
            placeholder[n-4]='0'
        else:
            placeholder[n-4]=''

    img.draw_string(30,50,placeholder[0]+str(time_data[4])+':'+placeholder[1]+str(time_data[5])+':'+placeholder[2]+str(time_data[6])\
    ,scale=8,x_spacing=1,mono_space=False)

    screen.display(img)
    time.sleep(0.05)
