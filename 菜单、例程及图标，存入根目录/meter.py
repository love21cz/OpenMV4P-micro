'''
OpenMV4P_micro
仪表展示程序

通过曲线图显示Y轴光流移动幅度
展现设备的可能性~

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220909
'''
import sensor, image, time,screen,button

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.B64X64)
sensor.set_windowing(32,64)
extra_fb = sensor.alloc_extra_fb(32,64, sensor.RGB565)#光流所需的画布

clock = time.clock()

screen=screen.screen()

img_now=sensor.alloc_extra_fb(320,172,sensor.RGB565)
img_last=sensor.alloc_extra_fb(320,172,sensor.RGB565)
y_now=172/2
y_last=172/2
cursor_y=0
while True:
    img = sensor.snapshot()
    displacement = extra_fb.find_displacement(img)
    extra_fb.replace(img)

    if displacement.response() > 0.1:
        y_now += displacement.y_translation()

    if y_now>170:
        y_now=170
    if y_now<2:
        y_now=2

    img_now.clear()
    img_now.draw_image(img_last,-5,0)
    img_now.draw_line(315,round(y_last),320,round(y_now))
    img_last.draw_image(img_now,0,0)
    img_now.draw_string(30,20,str(86-y_now),scale=2,x_spacing=1,mono_space=False)
    y_last=y_now

    print(y_now)
    screen.display(img_now,battery_icon=False)
