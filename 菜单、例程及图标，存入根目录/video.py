'''
OpenMV4P_micro
视频播放程序

仅作展示效果。所播放的.bin文件，可以通过OpenMV电脑端编程软件的工具-视频工具-转换视频文件生成。
仅支持视频，没有音频。

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20220909
'''

import sensor, image, time, screen

from pyb import millis

snapshot_source = False # Set to true once finished to pull data from sensor.

screen=screen.screen()

img = sensor.alloc_extra_fb(320,172, sensor.RGB565)#光流所需的画布

clock = time.clock()

stream = image.ImageIO("/KUN_30fps.bin", "r")

timer_start=millis()
timer=0
while(True):
    clock.tick()
    while millis()-timer_start-timer<33:
        pass
    timer+=33.33
    img = stream.read(copy_to_fb=True, loop=True, pause=True)
    screen.display(img,battery_icon=False)

    print(clock.fps())

